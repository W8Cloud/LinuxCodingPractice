#!/usr/bin/expect -f
set user ****
set host xxx.xxx.xxx.xxx
set port 22
set password ******
set timeout -1
spawn ssh $user@$host -p $port
expect "*password:"
send "$password\r"
interact
# Before using this script, remember to `chmod +x login.sh`