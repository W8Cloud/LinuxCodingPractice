# Initialize Gomobile
## Firstly
- You should install packages of Golang and build the environment of Golang
## Secondly
```
cd $GOROOT/src
mkdir -p golang.org/x/
``` 
## Thirdly
```
go get golang.org/x/mobile/cmd/Gomobile
./gomibile init
```
However, this operation didn't work and feedback 
```
no Android NDK path is set. Please run gomobile init with the ndk-bundle installed through the Android SDK manager or with the -ndk flag set 
```
Then I installed ndk-r16b in `/root/workspace/ndk/r16b/android-ndk-r16b`, which is also seted as $NDK_HOME
```
./gomobile init -ndk $NDK_HOME
```
Succeed!
## Test-example
```
./gomobile build -target=android golang.org/x/mobile/example/basic
adb install basic.apk
```
Succeed!