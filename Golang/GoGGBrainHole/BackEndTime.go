package BackEndTime

import (
	//"fmt"
	"time"
	"os"
	"log"
	//"encoding/gob"
)

func GetCurrentTime()(string,int,int,time.Month,int){
	formate:="2006-01-02 15:04:05 Mon"
	now:=time.Now()
	nowYear:=now.Year()
	nowyearlen:=365+int(bool2int8((nowYear%4==0)&&(nowYear%400!=0)))
	nowMonth:=now.Month()
	nowDay:=now.Day()
	local,err:=time.LoadLocation("PRC")
	if err!=nil{
		CheckErr(err,"GetCurrentTime")
	}
	return now.In(local).Format(formate),nowYear,nowyearlen,nowMonth,nowDay
}

func CountTime(timeNow string,timeSet string)(bool,int,int64,int64,int64){
	var flag bool
	var gap,tiN,tiS int64
	tN,err:=time.Parse("2006-01-02 15:04:05 Mon",timeNow)
	tS,err:=time.Parse("2006-01-02 15:04:05 Mon",timeSet)
	if err==nil {
		tiN=tN.Unix()
		tiS=tS.Unix()
		gap=tiN-tiS
		if tN.After(tS) {
			flag=true
		}else {
			flag=false
		}
	}else{
		CheckErr(err,"CountTime")
	}
	gapday:=int(gap/(60*60*24))
	return flag,gapday,gap,tiN,tiS
}

func CheckErr(err error,prob string){
	var fileName string
	fileName=prob+".log"
	logFile,err2:=os.Create(fileName)
	defer logFile.Close()
	if err2!=nil{
		log.Fatalln("GG! open file error!")
	}
	checkcheck:=log.New(logFile,"[Debug]",log.LstdFlags)
	checkcheck.Println("Wrong in function ",prob)
	checkcheck.SetPrefix("[Info]")
	checkcheck.Println("Can not work owing to ",err)
	checkcheck.SetFlags(checkcheck.Flags()| log.LstdFlags)
	checkcheck.Println("Different prefix")
	os.Exit(-1)
}

func bool2int8(b bool)int8{
	var ret int8
	if b==true{
		ret=1
	}else{
		ret=0
	}
	return ret
}

// func main(){
// 	var FirstDay string
// 	FirstDay="2018-03-08 00:13:14 Thu"
// 	PRCTime,_,_,_:=GetCurrentTime()
// 	_,gap_cnt,_,_:=CountTime(PRCTime,FirstDay)
// 	fmt.Println(gap_cnt)
// 	fmt.Println(PRCTime)
// }
func main(){
}