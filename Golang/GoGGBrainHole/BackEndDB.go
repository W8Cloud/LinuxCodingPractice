package BackEndDB

import(
	"fmt"
	"os"
	//"reflect"
	"log"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
) 

func DbConnect()(*sql.DB){
	db,err:=sql.Open("mysql","root:Njupt2018@tcp(114.67.242.239:3306)/note")
	if err!=nil{
		CheckErr(err,"DbConnect_sqlOpen")
		panic(err)
	}
	defer db.Close()
	return db
}

func DbInsert(db *sql.DB){
	var dateInsert string
	var eventInsert string
	fmt.Scanln(&dateInsert)
	fmt.Scanln(&eventInsert)
	stmt,err1:=db.Prepare("INSERT INTO note (date,something) VALUES (?,?)")
	if err1!=nil{
		CheckErr(err1,"DbInsert_Prepare")
	}
	defer stmt.Close()
	_,err2:=stmt.Exec(dateInsert,eventInsert)
	//fmt.Println(ret,reflect.TypeOf(ret))
	if err2!=nil{
		CheckErr(err2,"DbInsert_Exec")
	}
}

func DbUpdate(db *sql.DB){
	var dateUpdate string
	var eventUpdate string
	fmt.Scanln(&dateUpdate)
	fmt.Scanln(&eventUpdate)
	stmt,err1:=db.Prepare("UPDATE note SET date=? where something=?")
	if err1!=nil{
		CheckErr(err1,"DbUpdate_Prepare")
	}
	defer stmt.Close()
	_,err2:=stmt.Exec(dateUpdate,eventUpdate)
	if err2!=nil{
		CheckErr(err2,"DbUpdate_Exec")
	}
}

func DbDelete(db *sql.DB){
	var dateDelete string
	fmt.Scanln(&dateDelete)
	stmt,err1:=db.Prepare("DELETE FROM note where date=?")
	if err1!=nil{
		CheckErr(err1,"DbDelete_Prepare")
	}
	_,err2:=stmt.Exec(dateDelete)
	if err2!=nil{
		CheckErr(err2,"DbDelete_Exec")
	}
}

func DbAll(db *sql.DB){
	rows,err:=db.Query("SELECT * FROM note")
	if err!=nil{
		CheckErr(err,"DbAll")
	}
	dbListAll:=make(map[string]string)
	for rows.Next(){
		var date string
		var something string
		err=rows.Scan(&date,&something)
		if err!=nil{
			panic(err)
		}
		dbListAll[date]=something
	}
	fmt.Println(dbListAll)
}

func CheckErr(err error,prob string){
	var fileName string
	fileName=prob+".log"
	logFile,err2:=os.Create(fileName)
	defer logFile.Close()
	if err2!=nil{
		log.Fatalln("GG! open file error!")
	}
	checkcheck:=log.New(logFile,"[Debug]",log.LstdFlags)
	checkcheck.Println("Wrong in function ",prob)
	checkcheck.SetPrefix("[Info]")
	checkcheck.Println("Can not work owing to ",err)
	// checkcheck.SetFlags(checkcheck.Flags()| log.LstdFlags)
	// checkcheck.Println("Different prefix")
	os.Exit(-1)
}

func main(){
}