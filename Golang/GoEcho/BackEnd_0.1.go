package main

import (
	"fmt"
	//"reflect"
	//"strconv"
	"time"
	"os"
	"log"
	// "github.com/go-sql-driver/mysql"
	// "database/sql"
)

func GetCurrentTime()(string,int,time.Month,int){
	formate:="2006-01-02 15:04:05 Mon"
	now:=time.Now()
	nowYear:=now.Year()
	nowMonth:=now.Month()
	nowDay:=now.Day()
	local,err:=time.LoadLocation("PRC")
	if err!=nil{
		CheckErr(err,"GetCurrentTime")
	}
	return now.In(local).Format(formate),nowYear,nowMonth,nowDay
}

func CountTime(timeNow string,timeSet string)(string,bool,int64,int64,int64){
	var flag bool
	var gap,tiN,tiS int64
	ParseERROR:=""
	tN,err:=time.Parse("2006-01-02 15:04:05 Mon",timeNow)
	tS,err:=time.Parse("2006-01-02 15:04:05 Mon",timeSet)
	if err==nil {
		tiN=tN.Unix()
		tiS=tS.Unix()
		gap=tiN-tiS
		if tN.After(tS) {
			flag=true
		}else {
			flag=false
		}
	}else{
		CheckErr(err,"CountTime")
	}
	return ParseERROR,flag,gap,tiN,tiS
}

func CheckErr(err error,prob string){
	var fileName string
	fileName=prob+".log"
	logFile,err2:=os.Create(fileName)
	defer logFile.Close()
	if err2!=nil{
		log.Fatalln("GG! open file error!")
	}
	checktime:=log.New(logFile,"[Debug]",log.LstdFlags)
	checktime.Println("Wrong in function ",prob)
	checktime.SetPrefix("[Info]")
	checktime.Println("Can not work owing to ",err)
	checktime.SetFlags(checktime.Flags()| log.LstdFlags)
	checktime.Println("Different prefix")
	os.Exit(-1)
}

func main(){
	var FirstDay string
	FirstDay="2018-03-08 00:13:14 Thu"
	PRCTime,_,_,_:=GetCurrentTime()
	_,_,gap_cnt,_,_:=CountTime(PRCTime,FirstDay)
	fmt.Println(gap_cnt)
	fmt.Println(PRCTime)
}