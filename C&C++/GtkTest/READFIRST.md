## WHat is GTK?
```
GTK+, or the GIMP Toolkit, is a multi-platform toolkit for creating graphical user interfaces. 
Offering a complete set of widgets, GTK+ is suitable for projects ranging from small one-off tools to complete application suites.
```
[Click me to know more](https://www.gtk.org/)
## How to install?
```
sudo apt-get update
sudo apt-get install gnome-devel gnome-devel-docs
sudo apt-get install pkg-config
sudo apt-get install glade libglade2-dev
```
## How to build C/Cpp Source File?
```
gcc target.c -o target `pkg-config --cflags --libs gtk+-3`
``` 