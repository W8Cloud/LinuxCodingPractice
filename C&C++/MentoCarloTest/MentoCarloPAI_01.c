#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>

struct timespec tp;
int *xrand;
int *yrand;

void PRNG_Generate(int size,int samplenum);
int inCircle(int x,int y,int r);
int CountinCircle(int iflag,int pnt);
//double Calculate(int size,int pnt);

int main()
{
	int k,pointsnum,squsize,ret;	
	int pnt=0;
	double PI=0;
	puts("PLZ input the number of points & the size of the square");
	scanf("%d%d",&pointsnum,&squsize);
	PRNG_Generate(squsize,pointsnum);
	for(k=0;k<pointsnum;k++)
	{
		ret=inCircle(xrand[k],yrand[k],(float)squsize/2);
		pnt=CountinCircle(ret,pnt);
		//printf("%d %d\n",ret,pnt);
	};
	//PI=Calculate(pointsnum,pnt);
	printf("PI:%f\n",4.0*pnt/pointsnum);
	free(xrand);xrand=NULL;
	free(yrand);yrand=NULL;
	return 0;
};

void PRNG_Generate(int size,int samplenum)
{
	int i;
	xrand=(int*)malloc(samplenum*sizeof(int));
	yrand=(int*)malloc(samplenum*sizeof(int));
	clock_gettime(CLOCK_THREAD_CPUTIME_ID,&tp);
	srand(tp.tv_nsec);
	for(i=0;i<samplenum;i++)
	{
		xrand[i]=rand()%size;
		yrand[i]=rand()%size;
	};
};
int inCircle(int x,int y,int r)
{
	int flag=0;
	if(pow((double)(x-r),2)+pow((double)(y-r),2)<=pow((double)r,2))
		flag=1;
	return flag;
};
int CountinCircle(int iflag,int pnt)
{	
	if(iflag==1)pnt++;
	return pnt;
};
/*****************************************************************
double Calculate(int samplenum,int pnt)
{
	//printf("%f",4*(double)pnt/pow((double)size,2));	
	return 4.0*pnt/samplenum;
	printf("sigma = %f",samplenum);
};
*****************************************************************/
/*****************************************************************
BUG:GCC&G++ appeared to ignore function 'Calculate'
Solution:NULL
*****************************************************************/