#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>

struct timespec tp;
double *xrand;
double *yrand;

void PRNG_Generate(double size,int samplenum);
int inCircle(double x,double y,double r);
int CountinCircle(int iflag,int pnt);

int main()
{
	int k,pointsnum,ret;	
	int pnt=0;
	double squsize,PI=0;
	puts("PLZ input the number of points & the size of the square");
	scanf("%d%lf",&pointsnum,&squsize);
	PRNG_Generate(squsize,pointsnum);
	for(k=0;k<pointsnum;k++)
	{
		ret=inCircle(xrand[k],yrand[k],squsize/2.0);
		pnt=CountinCircle(ret,pnt);
	};
	printf("PI:%f\n",4.0*pnt/pointsnum);
};

void PRNG_Generate(double size,int samplenum)
{
	int i;
	xrand=(double*)malloc(samplenum*sizeof(double));
	yrand=(double*)malloc(samplenum*sizeof(double));
	clock_gettime(CLOCK_THREAD_CPUTIME_ID,&tp);
	srand(tp.tv_nsec);
	for(i=0;i<samplenum;i++)
	{
		xrand[i]=rand()/(double)(RAND_MAX/size);
		yrand[i]=rand()/(double)(RAND_MAX/size);
	};
};
int inCircle(double x,double y,double r)
{
	int flag=0;
	if(pow((x-r),2)+pow((y-r),2)<=pow(r,2))
		flag=1;
	return flag;
};
int CountinCircle(int iflag,int pnt)
{	
	if(iflag==1)pnt++;
	return pnt;
};
