# socket coding tips
## Minutiae
- variable `sin_size` is `socklen_t` not `int`
- function `close()` should be included in `unistd.h`
## Introduction to some critical functions
- [socket](http://blog.chinaunix.net/uid-20788470-id-1841640.html)
- [bind](http://blog.csdn.net/david_xtd/article/details/7090590)
- [listen](http://blog.csdn.net/u013920085/article/details/20574249)
- [connect](http://blog.csdn.net/lgp88/article/details/7171924)
- [accept](http://blog.csdn.net/stpeace/article/details/13424223)
- read()
- write()
- close()
## Structure of TCP
```
server: socket() --> bind() --> listen() --> accept() --> read()/recv()|write()/send() --> close()
                                               |                       ||
client: socket() -------------------------> connect() --> write()/send()|read()/recv() --> close()
```