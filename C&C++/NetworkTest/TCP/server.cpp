#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

struct sockaddr_in local_addr;
struct sockaddr_in remote_addr;

int main(int argc,char **argv)
{
    int server_sockfd;
    int client_sockfd;
    int len;
    socklen_t sin_size;
    char buf[BUFSIZ];
    local_addr.sin_family=AF_INET;
    local_addr.sin_addr.s_addr=INADDR_ANY;
    local_addr.sin_port=htons(8000);

    if((server_sockfd=socket(PF_INET,SOCK_STREAM,0))<0)
    {
        perror("SOCKET GG");
        return 1;
    }

    if(bind(server_sockfd,(struct sockaddr *)&local_addr,sizeof(struct sockaddr))<0)
    {
        perror("BIND GG");
        return 1;
    }

    if(listen(server_sockfd,5)<0)
    {
        perror("LISTEN GG");
        return 1;
    }

    sin_size=sizeof(struct sockaddr_in);

    if((client_sockfd=accept(server_sockfd,(struct sockaddr *)&remote_addr,&sin_size))<0)
    {
        perror("ACCEPT GG");
        return 1;
    }
    printf("accept client %s\n",inet_ntoa(remote_addr.sin_addr));
    len=send(client_sockfd,"You will be pwned!\n",19,0);

    while((len=recv(client_sockfd,buf,BUFSIZ,0))>0)
    {
        buf[len]='\0';
        printf("%s\n",buf);
        if(send(client_sockfd,buf,len,0)<0)
        {
            perror("WRITE GG");
            return 1;
        }
    }

    close(client_sockfd);
    close(server_sockfd);

    return 0;
}
